Simple zip file password cracker.

Usage:
Open terminal in project folder then type python cracker.py -f <file_name> -d <passwords> (Optionally -s to show current password checking)
You need to have installed python 2.7 installed on your machine, to check type: python --version
*you can also use pyhon 3 but zipfile module is different for this version on most computers

To create proper zipfile you can use FOR EXAMPLE this site:
https://freetoolonline.com/zip-file-with-password.html

Script in action:
![ScriptInAction](https://bytebucket.org/MichalWrobel/python-password-cracker-for-zip-files/raw/4673ddbe7a5879e1dec741d2423948b9813c27c9/ScreenShot.jpg)