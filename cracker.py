import zipfile
import optparse
import time
from threading import Thread

showEntries = False
bFound = False
def extractFile(zFile, password, entry):
	global showEntries
	global bFound
	if showEntries == True & bFound == False:
		print('[' + str(entry) + '] Trying: ' + password)

	try:
		zFile.extractall(pwd=password)
		bFound = True
		print('\x1b[6;30;42m' + '[+] Password found: ' + password + '\x1b[0m')
		exit()
	except:
		pass


def main():
	parser = optparse.OptionParser('Correct usage: <script> " + "-f <zipfile> -d <dictionary> Optional: -s (showing entries from dictionary)')
	parser.add_option('-f', '--file', dest='zname', type='string', help='specify zip file')
	parser.add_option('-d', '--dictionary', dest='dname', type='string', help='specify dictionary file')
	parser.add_option('-s', '--show', action='store_true')

	(options, args) = parser.parse_args()
	global bFound
	global showEntries
	showEntries = options.show is None

	if (len(options.zname) < 1 | len(options.dname) < 1):
		print (parser.usage)
		sys.exit()
	else:
		zname = options.zname
		dname = options.dname

	timeStart = time.time()
	zFile = zipfile.ZipFile(zname)
	passFile = open(dname)
	read = passFile.readlines()
	readLength = len(read)

	print('\n[*] Cracking for password to archive of: ' + '\n[*] Name: ' + zname + '\n[*] Length: ' + str(readLength) + '\n[*] Using: ' + dname + '\n')
	counter = 0
	for line in read:
		counter += 1
		password = line.strip('\n')
		extractFile(zFile, password, counter)

	if bFound is not True:
		print('\x1b[1;33;41m' + '[-] Not found' + '\x1b[0m')
	timeEnd = time.time()
	print('\n[*] Operation took: ' + str(round(timeEnd - timeStart, 3)) + ' seconds')

if __name__ == '__main__':
	main()